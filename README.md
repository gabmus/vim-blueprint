# vim-blueprint

Syntax highlighting for the [Blueprint markup language](https://gitlab.gnome.org/jwestman/blueprint-compiler).

## Install

As any other plugin. If you're using [Packer](https://github.com/wbthomason/packer.nvim/):

```lua
use 'https://gitlab.com/gabmus/vim-blueprint'
```
